<?php

namespace App\Http\Controllers\Admin\Post;

use App\Http\Resources\Post\PostResource;
use App\Models\Post;

class IndexController extends BaseController
{
    public function __invoke()
    {
        $posts = Post::all();
        return PostResource::collection($posts);
    }
}
