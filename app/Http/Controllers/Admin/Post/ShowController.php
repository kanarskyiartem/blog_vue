<?php

namespace App\Http\Controllers\Admin\Post;

use App\Http\Resources\Post\PostResource;
use App\Models\Post;

class ShowController extends BaseController
{
    public function __invoke(Post $post)
    {
        return new PostResource($post);
    }
}
