import Vue from 'vue'
import router from "./router";
import Index from './components/Index'
import Sidebar from './components/Sidebar'

require('./bootstrap');

const app = new Vue({
    el: '#app',

    components: {
        Index,
        Sidebar,
    },
    router,
});
