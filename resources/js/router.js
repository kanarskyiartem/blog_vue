import Vue from 'vue';
import VueRouter from "vue-router";

Vue.use(VueRouter)

export default new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/admin/tags',
            component: () => import('./components/Tag/Index'),
            name: 'tags.index'
        },
        {
            path: '/admin/tags/create',
            component: () => import('./components/Tag/Create'),
            name: 'tags.create'
        },
        {
            path: '/admin/tags/:id/edit',
            component: () => import('./components/Tag/Edit'),
            name: 'tags.edit'
        },
        {
            path: '/admin/tags/:id/',
            component: () => import('./components/Tag/Show'),
            name: 'tags.show'
        },
        {
            path: '/admin/categories',
            component: () => import('./components/Category/Index'),
            name: 'categories.index'
        },
        {
            path: '/admin/categories/create',
            component: () => import('./components/category/Create'),
            name: 'categories.create'
        },
        {
            path: '/admin/categories/:id/edit',
            component: () => import('./components/category/Edit'),
            name: 'categories.edit'
        },
        {
            path: '/admin/categories/:id/',
            component: () => import('./components/category/Show'),
            name: 'categories.show'
        },
        {
            path: '/admin/posts',
            component: () => import('./components/Post/Index'),
            name: 'posts.index'
        },
        {
            path: '/admin/posts/create',
            component: () => import('./components/Post/Create'),
            name: 'posts.create'
        },
        {
            path: '/admin/posts/:id/edit',
            component: () => import('./components/Post/Edit'),
            name: 'posts.edit'
        },
        {
            path: '/admin/posts/:id/',
            component: () => import('./components/Post/Show'),
            name: 'posts.show'
        },
    ],
})
