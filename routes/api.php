<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Admin'], function (){
    Route::group(['namespace' => 'Tag', 'prefix'=>'tags'], function (){
        Route::post('/', 'StoreController');
        Route::get('/', 'IndexController');
        Route::get('/{tag}', 'ShowController');
        Route::patch('/{tag}', 'UpdateController');
        Route::delete('/{tag}', 'DeleteController');
    });
    Route::group(['namespace' => 'Category', 'prefix'=>'categories'], function (){
        Route::post('/', 'StoreController');
        Route::get('/', 'IndexController');
        Route::get('/{category}', 'ShowController');
        Route::patch('/{category}', 'UpdateController');
        Route::delete('/{category}', 'DeleteController');
    });
    Route::group(['namespace' => 'Post', 'prefix'=>'posts'], function (){
//        Route::post('/', 'StoreController');
        Route::get('/', 'IndexController');
        Route::get('/{post}', 'ShowController');
//        Route::patch('/{post}', 'UpdateController');
        Route::delete('/{post}', 'DeleteController');
    });
});

